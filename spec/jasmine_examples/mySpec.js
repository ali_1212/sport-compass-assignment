const request = require('supertest');
describe('test endpoints in server, for triangle', () => {

    let server = require('./../../main');
        beforeEach(()=> {

            server = require('./../../main');

        });
        afterEach(() => {
            server.close();
        });

    it('should check if server can check for equilateral triangle', (done)=>{


        request(server).get('/triangle/3/3/3').end((err, res) => {
            if(err){
                console.log('ERROR '+err)
            }
            expect(res.body.type).toEqual('Equilateral');
            done();
        });
        });


        it('should check if server can check for isosceles triangle', (done)=>{


            request(server).get('/triangle/3/3/4').end((err, res) => {
                if(err){
                    console.log('ERROR '+err)
                }
                expect(res.body.type).toEqual('Isosceles');
                done();
            });
            });

        it('should check if server can check for scalene triangle', (done)=>{


            request(server).get('/triangle/2/3/4').end((err, res) => {
                if(err){
                    console.log('ERROR '+err)
                }
                expect(res.body.type).toEqual('Scalene');
                done();
            });
            });
    });


