const express = require('express');
const app = express();
const bodyParser = require('body-parser')
const knex = require('knex')({
    client: 'mysql',
    connection: {
      host: '127.0.0.1',
      user: 'root',
      password: '131288',
      database: 'sport_compass_db'
    }
  })

app.use(express.static(__dirname+'/public'));
app.use(bodyParser.urlencoded({extended: true}));

const scalene = 'Scalene';
const isosceles = 'Isosceles';
const equilateral = 'Equilateral';
const incorrect = 'Incorrect';


// below are enpoints, that just send html pages
app.get('/', (req, res) => {
    res.sendFile(__dirname+'/public/home.html');
})

app.get('/trianglePage', (req,res) => {
    res.sendFile(__dirname+'/public/triangle.html');
});

app.get('/blogPage', (req,res) => {
    res.sendFile(__dirname+'/public/blog-overview.html');
});

app.get('/blogOptions', (req, res) => {
    res.sendFile(__dirname+'/public/blog-options.html')
});

app.get('/createBlog', (req, res) => {
    res.sendFile(__dirname+'/public/create-blog.html')
});

// endpoint for check triangle function
app.get('/triangle/:a/:b/:c', (req, res) => {

    const a = parseFloat(req.params.a);
    const b = parseFloat(req.params.b);
    const c = parseFloat(req.params.c);

    let coord1_y = 0;
    let coord1_x = 0;

    let coord2_y = 0;
    let coord2_x = 0;

    let coord3_y = 0;
    let coord3_x = 0;

    let pos_x = 0;
    let pos_y = 0;


    let triangle_type = '';
    let height = 0;

    // this object is sent to client, and tells the type, the coordinates for the triangles edges and coordinates
    // for the whole triangles position
    let triangleInfo = {
        type: '',
        coord_A: {x: 0, y: 0},
        coord_B: {x: 0, y: 0},
        coord_C: {x: 0, y: 0},
        pos_x: 0,
        pos_y: 0
    }

    // check for equilateral
    if(a == b && a == c && b == c) {

        height = Math.sqrt(Math.pow(c, 2)-Math.pow(a/2, 2));

         pos_x = 10 - (a/2);
         pos_y = 5 - (height/2);

         coord1_x = b/2;
         coord1_y = 0;

         coord2_x = 0;
         coord2_y = height;

         coord3_x = b;
         coord3_y = height;

         triangle_type = equilateral;
    }

    // check for isosceles
    else if(a == c && a !== b || c == b && c !== a || a == b && a !== c) {

        triangle_type = isosceles;

        // if a is the smallest side
        if(a < c && a < b) {
 
            height = Math.sqrt(Math.pow(b, 2)-Math.pow((a/2), 2));

            pos_x = 10 - (a/2);
            pos_y = 5 - (height/2);

            coord1_x = a/2;
            coord1_y = 0;

            coord2_x = 0;
            coord2_y = height;

            coord3_x = a;
            coord3_y = height;

            // if b is the smallest side
        }else if ( b < a && b <  c) {

            height = Math.sqrt(Math.pow(a, 2)-Math.pow((b/2), 2));


        pos_x = 10 - (b/2);
        pos_y = 5 - (height/2);

            coord1 = {x: b/2, y: 0};
            coord2 = {x: 0, y: height};
            coord3 = {x: b, y:  height};

            coord1_x = b/2;
            coord1_y = 0;

            coord2_x = 0;
            coord2_y = height;

            coord3_x = b;
            coord3_y = height;

            // if c is the smallest side
        }else if( c < a && c < b) {

            height = Math.sqrt(Math.pow(b, 2)-Math.pow((c/2), 2));

            pos_x = 10 - (c/2);
            pos_y = 5 - (height/2);

            coord1_x = c/2;
            coord1_y = 0;

            coord2_x = 0;
            coord2_y = height;

            coord3_x = c;
            coord3_y = height;
        }
    }
    // check for scalene
    else if (a !== c && a !== b && b !== c && b !== a ) {

        let perimeter = 0;
        let area = 0;
        
        // finds angle A
        let cosine_A = (Math.pow(b, 2)+ Math.pow(c, 2) - Math.pow(a , 2))/(2*b*c);
        let angle_A = Math.acos(cosine_A)*(180/Math.PI);

        triangle_type = scalene;

        // checks if angle A is a valid number
        if(angle_A >= 180 || isNaN(angle_A)) {
            triangleInfo.type = incorrect;
            res.send(triangleInfo);
            return;
        }

        // if side a is the largest. Note that this is the only scenario the program can handle at this point
        // with regards to scalene triangles
        if(a > c && a > b ) {

            // checks if angle A is less than 90 degrees
            if(angle_A < 90) {

                perimeter = 0.5 * (a+b+c);
                area = Math.sqrt(perimeter*(perimeter-a)*(perimeter-b)*(perimeter-c));
                height = (area*2)/a;

                pos_x = 10 - (a/2);
                pos_y = 5 - (height/2);

                coord1_x = Math.sqrt(Math.pow(b, 2)-Math.pow(height,2));
                coord1_y = 0;

                coord2_x = 0;
                coord2_y = height;

                coord3_x = a;
                coord3_y = height;

                // check if angle A is between 90 and 180
            } else if(angle_A > 90 && angle_A < 180) {

                perimeter = 0.5 * (a+b+c);
                area = Math.sqrt(perimeter*(perimeter-a)*(perimeter-b)*(perimeter-c));
                height = (area*2)/a;

                coord1_x = Math.sqrt(Math.pow(b,2)-Math.pow(height,2));
                coord1_y = 0;

                coord2_x = 0;
                coord2_y = height;

                coord3_x = a;
                coord3_y = height;

                pos_x = 10 - (a/2);
                pos_y = 5 - (height/2);

                // checks if angle A is 90 degrees
            } else if(angle_A == 90) {


                coord1_x = 0;
                coord1_y = 0;

                coord2_x = 0;
                coord2_y = c;

                coord3_x = b;
                coord3_y = c;

                pos_x = 10 - (a/2);
                pos_y = 5 - (height/2);

            }

        }else if ( b > a && b >  c) {

        }else if( c > a && c > b) {

        }
    }

    triangleInfo = {
        type: triangle_type,
        coord_A: {x: coord1_x, y:coord1_y},
        coord_B: {x: coord2_x, y: coord2_y},
        coord_C: {x: coord3_x, y: coord3_y},
        pos_x: pos_x,
        pos_y: pos_y
    }

    res.send(triangleInfo);
});


// below are endpoints for blog function, that handle CRUD related requests

app.post('/createPost', (req, res) => {

    const date = new Date();

    const day = date.getDate();
    const month = date.getMonth()+1;
    const year = date.getFullYear();

    const currentDate = year+'-'+month+'-'+day;

    knex('user').where({username: 'Ali', password: '1234'}).then(res => {
        const row = res[0];

        return knex('blog').where({user_id: row.user_id}).then();
    }).then(row => {
        const blogRow = row[0];
        return knex('blog').insert({user_id: blogRow.user_id, blog_title: req.body.title,
        blog_text: req.body.blogText, creation_date: currentDate, username: blogRow.username}).then();
    }).then(result => {
        console.log(result);
        res.sendFile(__dirname+'/public/blog-overview.html');
    }).catch(err => {
        console.log(err);
    });
});

app.get('/readPosts', (req, res) => {
    const username = req.headers.user;
    const password = req.headers.password;

    knex('user').where({username: username, password: password}).then(userData => {

        return knex('blog').where({user_id: userData[0].user_id}).then();
    }).then(blogInfo => {
        
        res.send(blogInfo);
    }).catch(err => {
        console.log(err);
    });
});

app.put('/updatePost/:blogID', (req, res) => {
    
    const data = req.body;
    const blog_ID = req.params.blogID;


    knex('blog').where({id: blog_ID}).update({blog_title: data.blogTitle, blog_text: data.blogContent}).then(result => {
        res.sendStatus(200);
    }).catch(err => {
        console.log(err);
    })
    
});

app.delete('/deletePost/:blogID', (req, res) => {
    const blog_ID = req.params.blogID;

    knex('blog').where({id: blog_ID}).delete().then(result => {

        res.sendStatus(200);
    }).catch(err => {
        console.log(err);
    })
});


// application runs on port 1234
const server = app.listen(1234);

// for testing
module.exports = server;